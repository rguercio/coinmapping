# pip3 install scipy
import scipy.io as sio
import numpy as np
from numpy.linalg import matrix_rank
import matplotlib.pyplot as plot

FACTOR = 2/3
def plot_data(lambda_, J, match):
    fig_0 = plot.figure(1)
    # Display grid
    plot.grid(True, which="both")
    # Linear X axis, Logarithmic Y axis
    plot.semilogx(lambda_,J)
    #plot.ylim([10,21000])
    #plot.xlim([1900,2020])

    # Provide the title for the semilog plot
    plot.title('Erro Quadrático Médio com os dados de treinamento (40.000)')

    # Give x axis label for the semilog plot
    plot.xlabel('Coeficiente de regularização')

    # Give y axis label for the semilog plot
    plot.ylabel('Error J(w)')

    fig_0 = plot.figure(2)
    # Display grid
    plot.grid(True, which="both")
    # Linear X axis, Logarithmic Y axis
    plot.semilogx(lambda_,match)
    #plot.ylim([10,21000])
    #plot.xlim([1900,2020])

    # Provide the title for the semilog plot
    plot.title('Taxa de classificação correta com os dados de treinamento (40.000)')

    # Give x axis label for the semilog plot
    plot.xlabel('Coeficiente de regularização')

    # Give y axis label for the semilog plot
    plot.ylabel('Porcentagem de acerto (#)')

    # Display the semilog plot
    plot.show()

def load(filename):
    # Load the data
    mat_contents = sio.loadmat(filename)
    return mat_contents

def extract_input(data):
    return data['X']

def extract_output(data):
    return data['S']

def define_training_data(data, factor):
    # factor from 0 to 1
    shape_ = data.shape # [Lines / Columns]
    number_of_lines = int(shape_[0]*factor)
    return data[:number_of_lines, :], data[number_of_lines:, :]

def create_polarization_matrix(data_training, enable_neural_network=True):
    
    ones = np.ones((data_training.shape[0],1))
    polar = []
    if (enable_neural_network):
        number_of_neurals = 500
        # H inicial Nxn (Número total de amostras x Número de neurônios) 60000 x 500
        # H = f(v,b,x) -> X entrada | v poderacao | b limiar
        # Pesos devem ser distribuídos aleateoriamente com D.N e D.P .2
        mean, sigma = 0, 0.2 # mean and standard deviation
        v = np.random.normal(mean, sigma, (data_training.shape[1],number_of_neurals)) #% 784 x 500
        b = np.random.normal(mean, sigma, (1,number_of_neurals)) #% 1 x 500
        vb = np.concatenate((v, b), axis=0) #% 785 x 500
        u = np.zeros((data_training.shape[0],number_of_neurals))
        for i in range(0, data_training.shape[0]):
            u[i,:] = np.matmul(np.concatenate((data_training[i,:], np.ones(1)), axis=0),vb)  #% (40000, 784) (785 x 500)
        pre_polar = np.divide(np.ones(u.shape), np.ones(u.shape) + np.exp(-u))
        polar = np.concatenate((ones, pre_polar), axis=1)

    else:
        polar = np.concatenate((ones, data_training), axis=1)
        rank = matrix_rank(polar)
    
        if (rank < data_training.shape[0] and rank < data_training.shape[1]):
            print(rank)
            print("It does not have full rank")
    return polar

def create_model(data_input_training, data_output_training, polar_matrix):
    N = data_input_training.shape[0] # number_of_samples
    print("number_of_samples: ", N)
    resolution = polar_matrix.shape[1]
    print("resolution: ", resolution)
    K = data_output_training.shape[1] # number_of_output
    print("number_of_outputs", K)
    W = np.zeros((resolution, K))

    coef = np.linspace(-10, 13, num=24)
    lambda_ = 2**(coef)
    J = np.zeros(lambda_.shape) # Error
    y = np.zeros((data_output_training.shape)) # Classification output
    match = np.zeros(lambda_.shape) # Count of hits
    print("lambda_: ", lambda_)
   
    for i in range(1,len(lambda_)):
        #Resolvendo sistemas lineares sobredeterminados 
         #W = (( (X_t_polar') * X_t_polar + lambda(i)*eye(Nn) ) ^ (-1)) * (X_t_polar') * S_t #lambda > 0 
        P0 = np.matmul( np.transpose(polar_matrix) , polar_matrix )
        P1 = lambda_[i]*np.eye(resolution)
        P2 = P0 + P1
        P3 = np.linalg.inv(P2)
        P4 = np.matmul(np.transpose(polar_matrix), data_output_training )
        W = np.matmul(P3,P4)
        #Caso 1: Erro Quadrático Médio
        #Quadrados mínimos com regularização
        #Norma euclidiana do erro ao quadrado
        K0 = np.matmul(polar_matrix,W)
        K1 = K0 - data_output_training
        K2 = np.linalg.norm(K1)
        K3 = np.linalg.norm(W)
        #J[i] = K2 + K3
        J[i] = K2*K2 + lambda_[i]*K3*K3
        #Caso 2: Taxa de erro de classificação
        y = K0 #Matriz N_X_t x 10 

        #Analisando amostra por amostra
        for k in range(1, N): #k = 1:N_X_t Linhas = 60000 e Colunas = 10
            y_c = y[k,:] #Amostra ou linha 1x10
            max_y_c = np.amax(y_c) #Valor máximo do amostra [0,1]
            y_c[y_c!=max_y_c] = 0 # Se não for o maior, faça zero
            y_c = y_c/max_y_c # Faça o maior ser 1
            if all(y_c == data_output_training[k,:]): # Verificando o resultado
                match[i] = match[i] + 1

    match = match/N

    lambdaEQM = [x for i, x in enumerate(lambda_) if J[i] == min(J)] #lambda_(find(J == min(J)))
    print('Melhor lambda para E.Q.M : ', lambdaEQM)

    lambdaTCC = [x for i, x in enumerate(lambda_) if match[i] == max(match)] 
    print('Melhor lambda para T.C.C : ', lambdaTCC)

    bestTCC = max(match)
    print('Melhor T.C.C : ', bestTCC)

    plot_data(lambda_,J, match)
    #return lambda_, match, J
        
if __name__=="__main__":
    name_data = 'data.mat'
    all_data = load(name_data)
    input_ = extract_input(all_data)
    output_ = extract_output(all_data)
    data_input_training_first, data_input_training_second = define_training_data(input_,FACTOR)
    data_output_training_first, data_output_training_second = define_training_data(output_,FACTOR)
    print(data_input_training_first.shape)
    polar_matrix_first = create_polarization_matrix(data_input_training_first)
    create_model(data_input_training_first,data_output_training_first, polar_matrix_first)
    print(data_input_training_second.shape)
    polar_matrix_second = create_polarization_matrix(data_input_training_second)
    create_model(data_input_training_second,data_output_training_second, polar_matrix_second)