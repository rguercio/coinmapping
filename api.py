import requests
import json
import json, hmac, hashlib, time, requests
from requests.auth import AuthBase
from datetime import datetime, timedelta

ENDPOINT = 'https://api.coinbase.com/v2/'

HEADERS_POST = {
    "Accept": "application/json",
    "Content-Type": "application/json"
}

HEADERS_GET = {"Accept": "application/json"}

# Create custom authentication for Coinbase API
class CoinbaseWalletAuth(AuthBase):
    def __init__(self, api_key, secret_key):
        self.api_key = api_key
        self.secret_key = secret_key

    def __call__(self, request):
        timestamp = str(int(time.time()))
        message = timestamp + request.method + request.path_url + (request.body or '')
        signature = hmac.new(self.secret_key, message, hashlib.sha256).hexdigest()

        request.headers.update({
            'CB-ACCESS-SIGN': signature,
            'CB-ACCESS-TIMESTAMP': timestamp,
            'CB-ACCESS-KEY': self.api_key,
        })
        return request
            
def GET(url, command):
    print(url + command)
    response = requests.request("GET", url + command, headers=HEADERS_GET)
    return response.text

def POST(url):
    response = requests.request("POST", url, headers=HEADERS_POST)
    return response.text

def GetCurrencies():
    # List known currencies.
    return GET(ENDPOINT,'currencies')

def GetExchangeRates(): 
    #  Default base currency is USD
    return GET(ENDPOINT,'exchange-rates')

def GetBuyPrice(source, dest):
    # Get the total price to buy one bitcoin or ether.
    #  This buy price includes standard Coinbase fee (1%) but excludes any other fees including bank fees.
    command = 'prices/' + source + '-' + dest + '/buy'
    # see buy bitcoin endpoint and quote: true option.
    return GET(ENDPOINT, command)

def GetSellPrice(source, dest):
    # Get the total price to buy one bitcoin or ether.
    #  This buy price includes standard Coinbase fee (1%) but excludes any other fees including bank fees.
    command =  'prices/' + source + '-' + dest + '/sell'
    # see sell bitcoin endpoint and quote: true option.
    return GET(ENDPOINT, command)

def GetSpotPrice(source, dest):
    # Get the current market price for bitcoin for example. This is usually somewhere in between the buy and sell price.
    command = 'prices/' + source + '-' + dest + '/spot'
    return GET(ENDPOINT, command)

def GetSpotPriceOnData(source, dest, date):
    # Get the current market price for bitcoin. This is usually somewhere in between the buy and sell price.
    command = 'prices/' + source + '-' + dest + '/spot?date=' + date
    return GET(ENDPOINT, command)

def GetCurrentTime():
    # Get the API server time.
    #  Default base currency is USD
    return GET(ENDPOINT,'time')

def GoToNextDay(date_str):
    year = int(date_str[0:4])
    month = int(date_str[5:7])
    day = int(date_str[8:10])
    date = datetime(year, month, day)
    date += timedelta(days=1)
    return str(date)[0:10]

'''
Excel: version 1
TAB: Coin Name
Column A: Date
Column B: Buy price
Column C: Sell price

Excel: version 2
TAB: Coin Name
Column A: Date
Column B: SpotPrice
'''

if __name__=="__main__":
    print(GetSpotPriceOnData('BTC','USD','2022-02-10'))
    print(GetSpotPriceOnData('BTC','USD','2021-02-11'))
    print(GoToNextDay('2022-02-10'))

# https://docs.cloud.coinbase.com/exchange/reference/exchangerestapi_getproducts